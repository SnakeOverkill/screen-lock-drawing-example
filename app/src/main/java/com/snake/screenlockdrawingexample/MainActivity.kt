package com.snake.screenlockdrawingexample

import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.os.Build
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.widget.SeekBar
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.snake.drawingview.DrawingView
import com.snake.screenlockdrawingexample.databinding.ActivityMainBinding
import java.io.ByteArrayOutputStream
import java.util.zip.DeflaterOutputStream
import java.util.zip.InflaterOutputStream
import kotlin.jvm.internal.Intrinsics


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        binding.drawingView.setBrushDrawingMode(true, binding.ivUndoBg)

        binding.ivUndoBg.setOnClickListener {
            if (binding.drawingView.drawShapes.size > 0) {
                binding.drawingView.undo()
                if (binding.drawingView.drawShapes.size == 0) {
                    binding.ivUndoBg.setColorFilter(getColor(R.color.colorWhiteAlpha))
                    binding.tvTitle.text = getString(R.string.brush_size)
                    binding.ivEraser.setImageResource(R.drawable.ic_eraser)
                    if (binding.drawingView.isErasing) {
                        setupDrawing()
                    }
                }
                if (binding.drawingView.redoShapes.size > 0) {
                    binding.ivRedoBg.setColorFilter(getColor(R.color.colorBlack))
                }
            }
        }

        binding.ivRedoBg.setOnClickListener {
            if (binding.drawingView.redoShapes.size > 0) {
                binding.drawingView.redo()
                if (binding.drawingView.drawShapes.size > 0) {
                    binding.ivUndoBg.setColorFilter(getColor(R.color.colorBlack))
                }
                if (binding.drawingView.redoShapes.size == 0) {
                    binding.ivRedoBg.setColorFilter(getColor(R.color.colorWhiteAlpha))
                }
            }
        }

        binding.lnBrush.setOnClickListener {
            setupDrawing()
        }

        binding.lnEraser.setOnClickListener {
            setEraser()
        }

        val defaultSize = DrawingView.DEFAULT_SIZE.toInt()
        binding.sbSize.progress = defaultSize

        binding.sbSize.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.drawingView.setBrushOrEraserSize(progress.toFloat())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })

        binding.tvTitle.setOnClickListener {
            drawCanvas()
        }

    }

    fun drawCanvas() {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_brush_active)
        val canvas = Canvas(bitmap)
        val shapes = binding.drawingView.drawShapes[binding.drawingView.drawShapes.size - 1]
        shapes?.shape?.draw(canvas, shapes.paint)
        binding.drawingView.invalidate()
    }

    private fun setupDrawing() {
        if (binding.drawingView.isErasing) {
            binding.drawingView.enableDrawing(true)
            binding.tvTitle.text = getString(R.string.brush_size)
            binding.ivEraser.setImageResource(R.drawable.ic_eraser)
            binding.ivBrush.setImageResource(R.drawable.ic_brush_active)
            binding.tvFreeHand.setTextColor(getColor(R.color.colorRed))
            binding.tvEraser.setTextColor(getColor(R.color.colorBlack))
        }
    }

    private fun setEraser() {
        if (!binding.drawingView.isErasing && binding.drawingView.drawShapes.size > 0) {
            binding.drawingView.brushEraser()
            binding.ivBrush.setImageResource(R.drawable.ic_brush)
            binding.ivEraser.setImageResource(R.drawable.ic_eraser_active)
            binding.tvTitle.text = getString(R.string.eraser_size)
            binding.tvEraser.setTextColor(getColor(R.color.colorRed))
            binding.tvFreeHand.setTextColor(getColor(R.color.colorBlack))
        }
    }

    private fun compress(bArr: ByteArray): ByteArray {
        return try {
            val byteArrayOutputStream = ByteArrayOutputStream()
            val deflaterOutputStream = DeflaterOutputStream(byteArrayOutputStream)
            deflaterOutputStream.write(bArr)
            deflaterOutputStream.flush()
            deflaterOutputStream.close()
            val byteArray = byteArrayOutputStream.toByteArray()
            byteArray
        } catch (e: Exception) {
            e.printStackTrace()
            ByteArray(0)
        }
    }

    private fun decompress(bArr: ByteArray): ByteArray {
        return try {
            val byteArrayOutputStream = ByteArrayOutputStream()
            val inflaterOutputStream = InflaterOutputStream(byteArrayOutputStream)
            inflaterOutputStream.write(bArr)
            inflaterOutputStream.flush()
            inflaterOutputStream.close()
            val byteArray = byteArrayOutputStream.toByteArray()
            byteArray
        } catch (e: Exception) {
            e.printStackTrace()
            ByteArray(0)
        }
    }

    fun toByteArray(parcelable: Parcelable?): ByteArray {
        var bArr: ByteArray
        val obtain = Parcel.obtain()
        return try {
            try {
                obtain.writeParcelable(parcelable, 0)
                val byteArray = obtain.marshall()
                bArr = compress(byteArray)
                Log.d("vvsset", "toByteArray: " + byteArray.size + "  " + bArr.size)
            } catch (unused: Exception) {
                bArr = ByteArray(0)
            }
            bArr
        } finally {
            obtain.recycle()
        }
    }

//    fun toModel(bArr: ByteArray): Data? {
//        val data: Data
//        val obtain = Parcel.obtain()
//        val decompress = decompress(bArr)
//        return try {
//            obtain.unmarshall(decompress, 0, decompress.size)
//            obtain.setDataPosition(0)
//            data = if (Build.VERSION.SDK_INT >= 33) {
//                obtain.readParcelable(
//                    Data::class.java.getClassLoader(),
//                    Data::class.java
//                ) as Data?
//            } else {
//                obtain.readParcelable<Parcelable>(Data::class.java.getClassLoader()) as Data?
//            }
//            obtain.recycle()
//            data
//        } catch (unused: Exception) {
//            obtain.recycle()
//            null
//        } catch (th: Throwable) {
//            obtain.recycle()
//            throw th
//        }
//    }


}