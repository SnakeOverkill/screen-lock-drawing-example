package com.snake.drawingview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.util.AttributeSet
import android.util.Log
import android.util.Pair
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import androidx.annotation.IntRange
import androidx.core.graphics.createBitmap
import java.util.*
import kotlin.math.log

/**
 *
 *
 * This is custom drawing view used to do painting on user touch events it it will paint on canvas
 * as per attributes provided to the paint
 *
 *
 * @author [Burhanuddin Rashid](https://github.com/burhanrashid52)
 * @version 0.1.1
 * @since 12/1/18
 */
class DrawingView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : View(context, attrs, defStyle) {
    val drawShapes = Stack<ShapeAndPaint?>()
    val redoShapes = Stack<ShapeAndPaint?>()
    internal var currentShape: ShapeAndPaint? = null
    var isDrawingEnabled = true
        private set
    var currentShapeBuilder: ShapeBuilder

    // eraser parameters
    var isErasing = false
    private var ivUndo: ImageView? = null
    var canvas: Canvas? = null

    // endregion
    private fun createPaint(): Paint {
        val paint = Paint()
        paint.isAntiAlias = true
        paint.isDither = true
        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeCap = Paint.Cap.ROUND
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC)

        // apply shape builder parameters
        currentShapeBuilder.apply {
            paint.strokeWidth = this.shapeSize
            // 'paint.color' must be called before 'paint.alpha',
            // otherwise 'paint.alpha' value will be overwritten.
            paint.color = this.shapeColor
            shapeOpacity?.also { paint.alpha = it }
        }

        return paint
    }

    private fun createEraserPaint(): Paint {
        val paint = createPaint()
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        return paint
    }

    fun setShapeColor(color: Int) {
        if (drawShapes.size > 0) {
            for (i in 0 until drawShapes.size) {
                drawShapes[i]?.paint?.color = color
            }
            invalidate()
        }
    }

    fun clearAll() {
        drawShapes.clear()
        redoShapes.clear()
        invalidate()
    }

    public override fun onDraw(canvas: Canvas) {
        this.canvas = canvas
        for (shape in drawShapes) {
            shape?.shape?.draw(canvas, shape.paint)
        }
    }

    /**
     * Handle touch event to draw paint on canvas i.e brush drawing
     *
     * @param event points having touch info
     * @return true if handling touch events
     */
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (isDrawingEnabled) {
            val touchX = event.x
            val touchY = event.y
            when (event.action) {
                MotionEvent.ACTION_DOWN -> onTouchEventDown(touchX, touchY)
                MotionEvent.ACTION_MOVE -> onTouchEventMove(touchX, touchY)
                MotionEvent.ACTION_UP -> onTouchEventUp(touchX, touchY)
            }
            invalidate()
            true
        } else {
            false
        }
    }

    private fun onTouchEventDown(touchX: Float, touchY: Float) {
        createShape()
        currentShape?.shape?.startShape(touchX, touchY)
    }

    private fun onTouchEventMove(touchX: Float, touchY: Float) {
        currentShape?.shape?.moveShape(touchX, touchY)
    }

    private fun onTouchEventUp(touchX: Float, touchY: Float) {
        currentShape?.apply {
            shape.stopShape()
            endShape(touchX, touchY)
            if (drawShapes.size > 0) {
                ivUndo?.setColorFilter(context.getColor(R.color.colorBlack))
            }
        }
    }

    private fun createShape() {

        val shape = BrushShape()
        val paint = if (isErasing) {
            createEraserPaint()
        } else {
            createPaint()
        }

        currentShape = ShapeAndPaint(shape, paint)
        drawShapes.push(currentShape)
    }

    private fun endShape(touchX: Float, touchY: Float) {
        if (currentShape?.shape?.hasBeenTapped() == true) {
            // just a tap, this is not a shape, so remove it
            drawShapes.remove(currentShape)
            //handleTap(touchX, touchY);
        }
    }

    fun undo(): Boolean {
        if (!drawShapes.empty()) {
            redoShapes.push(drawShapes.pop())
            invalidate()
        }
        return !drawShapes.empty()
    }

    fun redo(): Boolean {
        if (!redoShapes.empty()) {
            drawShapes.push(redoShapes.pop())
            invalidate()
        }
        return !redoShapes.empty()
    }

    fun cloneDrawShapes() {
        if (!drawShapes.empty()) {
            drawShapes.pop()
            invalidate()
        }
    }

    // region eraser
    fun brushEraser() {
        isDrawingEnabled = true
        isErasing = true
    }

    fun enableDrawing(brushDrawMode: Boolean) {
        isDrawingEnabled = brushDrawMode
        isErasing = !brushDrawMode
        if (brushDrawMode) {
            visibility = VISIBLE
        }
    }

    fun setBrushDrawingMode(brushDrawingMode: Boolean, ivUndo: ImageView) {
        enableDrawing(brushDrawingMode)
        this.ivUndo = ivUndo
    }

    fun setOpacity(@IntRange(from = 0, to = 100) opacity: Int) {
        var opacityValue = opacity
        opacityValue = (opacityValue / 100.0 * 255.0).toInt()
        currentShapeBuilder.withShapeOpacity(opacityValue)
    }

    fun setBrushOrEraserSize(brushEraserSize: Float) {
        currentShapeBuilder.withShapeSize(brushEraserSize)
    }

    fun setBrushColor(brushColor: Int) {
        currentShapeBuilder.withShapeColor(brushColor)
    }

    // endregion
    val drawingPath: Pair<Stack<ShapeAndPaint?>, Stack<ShapeAndPaint?>>
        get() = Pair(drawShapes, redoShapes)

    companion object {
        const val DEFAULT_SIZE = 80.0f
    }

    init {
        setLayerType(LAYER_TYPE_HARDWARE, null)
        visibility = GONE
        currentShapeBuilder = ShapeBuilder()
    }
}