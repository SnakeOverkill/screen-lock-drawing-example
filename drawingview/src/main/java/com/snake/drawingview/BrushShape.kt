package com.snake.drawingview

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.util.Log
import kotlin.math.abs

open class BrushShape : Shape {
    private var path = Path()
    private var left = 0f
    private var top = 0f
    private var right = 0f
    private var bottom = 0f

    override fun draw(canvas: Canvas, paint: Paint) {
        canvas.drawPath(path, paint)
    }

    override fun startShape(x: Float, y: Float) {
        Log.d("tag===", "startShape@ $x,$y")
        path.moveTo(x, y)
        left = x
        top = y
    }

    override fun moveShape(x: Float, y: Float) {
        val dx = abs(x - left)
        val dy = abs(y - top)
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            path.quadTo(left, top, (x + left) / 2, (y + top) / 2)
            left = x
            top = y
        }
    }

    override fun stopShape() {
        Log.d("tag===", "stopShape")
    }

    private val bounds: RectF
        get() {
            val bounds = RectF()
            path.computeBounds(bounds, true)
            return bounds
        }

    fun hasBeenTapped(): Boolean {
        val bounds = bounds
        return bounds.top < TOUCH_TOLERANCE && bounds.bottom < TOUCH_TOLERANCE && bounds.left < TOUCH_TOLERANCE && bounds.right < TOUCH_TOLERANCE
    }

    override fun toString(): String {
        return ": left: " + left +
                " - top: " + top +
                " - right: " + right +
                " - bottom: " + bottom
    }

    companion object {
        const val TOUCH_TOLERANCE = 4f
    }

}