package com.snake.drawingview

import android.graphics.Paint

/**
 * Simple data class to be put in an ordered Stack
 */
open class ShapeAndPaint(
    val shape: BrushShape,
    val paint: Paint
)